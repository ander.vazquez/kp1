package exam;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith (value = Parameterized.class)
public class testExam {
	int n;
	boolean exp;
	
	@Parameters
	public static Collection<Object[]> numbers(){
		return Arrays.asList(new Object[][] {
			{3, true},
			{0, false},
			{1, false},
			{21, false}
		}
		);
	}
	
	public testExam(int n, boolean exp) {
		this.n = n;
		this.exp = exp;
	}
	
	
	@Test
	public void test() {
		Exam1 ex = new Exam1();
		assertEquals(ex.exam(n), exp);		
	}

}
